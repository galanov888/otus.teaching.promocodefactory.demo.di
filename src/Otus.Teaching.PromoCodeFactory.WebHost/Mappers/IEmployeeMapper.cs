﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public interface IEmployeeMapper
    {
        Task<Employee> MapFromModelAsync(CreateOrEditEmployeeRequest model,
            Employee employee = null);
    }
}